# CSS Icons

## Use icon entirely made in CSS

![Icons screenshot](/screenshot.png)

## Class list

* account
* cat
* pc
* hamburger
* message
* close
* add
* minus
* search
* heart
* male
* female
* shutdown
* disabled
* reload
* wifi
* arrowLeft
* arrowRight
* arrowUp
* arrowDown
* checked
* play
* pause
* stop
* next
* previous
* document
* edit
* trash
* music
* home
* threePoints
* chat
* keyboard
* mouse
* grid
* note
* invader
* location
* camera
* time
* smartphone
* laptop
* lock
* micro
* sound
* mute
* equalizer
* code
* hashtag
* chip
* git
* batteryFull
* batteryHalf
* batteryEmpty
* info
* notification
* bookmark
